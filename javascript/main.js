$(document).ready(function() {

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    $('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function(){
        $(this).toggleClass('open');
    });


    $('.pe-7s-close').click(function () {
        $('.header-menu').toggle();
        $('.menu-btn').toggle();
    });


    $(".menu-btn").click(function (e) {
        $('.header-menu').toggle();
        $('.menu-btn').toggle();
    });

    $(".main-first-div1").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });


    $(".footer-rocket").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });

    $('.main-second2-div4-flex').find('.main-second2-div4-flex-menuitem').first().find('.menu-span').find('span').css("display","block");

    $('.main-second2-div4-flex-menuitem span a').click(function (event) {
        event.preventDefault();


        $(this).parent().parent().parent().find('.main-second2-div4-flex-menuitem').each(function () {
            $(this).find('.menu-span').find('span').css("display","none");
        });

        $(this).parent().parent().find('.menu-span').find('span').css("display","block");


    });

    $('#PROJECTS').click(function () {
        $('.main-second2-div4-images-flex').find('img:eq(0)').first().attr('src', 'images/grid1.jpg')
        $('.main-second2-div4-images-flex').find('img:eq(1)').first().attr('src', 'images/grid2.jpg')
        $('.main-second2-div4-images-flex').find('img:eq(2)').first().attr('src', 'images/grid3.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(0)').first().attr('href', 'images/grid1.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(1)').first().attr('href', 'images/grid2.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(2)').first().attr('href', 'images/grid3.jpg')
    });

    $('#PHOTOGRAPHY').click(function () {
        $('.main-second2-div4-images-flex').find('img:eq(0)').first().attr('src', 'images/grid4.jpg')
        $('.main-second2-div4-images-flex').find('img:eq(1)').first().attr('src', 'images/grid5.jpg')
        $('.main-second2-div4-images-flex').find('img:eq(2)').first().attr('src', 'images/grid6.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(0)').first().attr('href', 'images/grid4.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(1)').first().attr('href', 'images/grid5.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(2)').first().attr('href', 'images/grid6.jpg')
    });

    $('#DESIGN').click(function () {
        $('.main-second2-div4-images-flex').find('img:eq(0)').first().attr('src', 'images/grid7.jpg')
        $('.main-second2-div4-images-flex').find('img:eq(1)').first().attr('src', 'images/grid8.jpg')
        $('.main-second2-div4-images-flex').find('img:eq(2)').first().attr('src', 'images/grid9.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(0)').first().attr('href', 'images/grid7.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(1)').first().attr('href', 'images/grid8.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(2)').first().attr('href', 'images/grid9.jpg')
    });

    $('#BRANDING').click(function () {
        $('.main-second2-div4-images-flex').find('img:eq(0)').first().attr('src', 'images/grid1.jpg')
        $('.main-second2-div4-images-flex').find('img:eq(1)').first().attr('src', 'images/grid2.jpg')
        $('.main-second2-div4-images-flex').find('img:eq(2)').first().attr('src', 'images/grid3.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(0)').first().attr('href', 'images/grid1.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(1)').first().attr('href', 'images/grid2.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(2)').first().attr('href', 'images/grid3.jpg')
    });

    $('#MOBILE').click(function () {
        $('.main-second2-div4-images-flex').find('img:eq(0)').first().attr('src', 'images/grid4.jpg')
        $('.main-second2-div4-images-flex').find('img:eq(1)').first().attr('src', 'images/grid5.jpg')
        $('.main-second2-div4-images-flex').find('img:eq(2)').first().attr('src', 'images/grid6.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(0)').first().attr('href', 'images/grid4.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(1)').first().attr('href', 'images/grid5.jpg')
        $('.main-second2-div4-images-flex').find('a:eq(2)').first().attr('href', 'images/grid6.jpg')
    });

    $('.main-eight-link').click(function (event) {
            event.preventDefault();
            $('.main-eight-map').toggle();
        }
    );

    $('.owl-carousel').owlCarousel({
        dots: true,
        navigation : false,
        items : 1
    });
});

// Get the modal
var modal1 = document.getElementById('myModal1');
var modal2 = document.getElementById('myModal2');
var modal3 = document.getElementById('myModal3');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img1 = document.getElementById('myImg1');
var img2 = document.getElementById('myImg2');
var img3 = document.getElementById('myImg3');

var modalImg1 = document.getElementById("img01");
var modalImg2 = document.getElementById("img02");
var modalImg3 = document.getElementById("img03");

var captionText = document.getElementById("caption");
img1.onclick = function(){
    modal1.style.display = "block";
    modalImg1.src = this.src;
    captionText.innerHTML = this.alt;
}

img2.onclick = function(){
    modal2.style.display = "block";
    modalImg2.src = this.src;
    captionText.innerHTML = this.alt;
}

img3.onclick = function(){
    modal3.style.display = "block";
    modalImg3.src = this.src;
    captionText.innerHTML = this.alt;
}
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal1.style.display = "none";
    modal2.style.display = "none";
    modal3.style.display = "none";
}

function closeNav() {
    document.getElementById("side-nav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    document.body.style.backgroundColor = "white";
}

function openNav() {
    if (document.getElementById("side-nav").style.width == "" || document.getElementById("side-nav").style.width == "0px")
    {
        document.getElementById("side-nav").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    }
    else
    {
        closeNav();
    }

}
